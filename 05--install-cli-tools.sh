apps=(
  # Enter the list of applications here by separating them with an enter
  
  # For example:
  # coreutils
  # zsh
)

echo 'Installing CLI tools...'
brew install ${apps[@]}
echo 'Cleaning...'
brew cleanup
# Change shell to ZSH
# echo 'Changing shell assigned to your user account:'
# echo "/usr/local/bin/zsh" >> /etc/shells
# chsh -s /usr/local/bin/zsh
echo 'Done!'
echo 'Installation completed!'
